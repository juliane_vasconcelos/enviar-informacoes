package br.com.galgo.enviar;

import org.junit.Assert;

import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioExtrato;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioExtratoArquivo;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioInfoAnbimaPortal;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPlCota;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPlCotaArquivo;
import br.com.galgo.testes.recursos_comuns.pageObject.envio.TelaEnvioPosicaoArquivo;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;

public class EnviarInformacoes extends TelaGalgo {

	private Teste teste;
	private String caminhoMassaDadosFundos;

	public void enviarInformacao(Teste teste) {
		this.teste = teste;
		Ambiente ambiente = teste.getAmbiente();

		caminhoMassaDadosFundos = MassaDados.fromAmbiente(ambiente,
				ConstantesTestes.DESC_MASSA_DADOS_TRANSF).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		enviarInformacao(usuario);
	}

	public void enviarInformacao(Usuario usuario) {
		Ambiente ambiente = teste.getAmbiente();
		Servico servico = teste.getServico();
		Canal canal = teste.getCanal();
		int qtdReteste = teste.getQtdReteste();
		TelaGalgo.abrirBrowser(ambiente.getUrl());
		TelaLogin telaLogin = new TelaLogin();
		telaLogin.loginAs(usuario);

		int qtdExecucoes = 1;
		while (qtdExecucoes <= qtdReteste) {
			String codigoSTI = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 1, qtdExecucoes);

			String dataBase = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, 2, qtdExecucoes);

			String cotista = ArquivoUtils.getValorColuna(teste,
					caminhoMassaDadosFundos, false, 3, qtdExecucoes);

			enviar(usuario, servico, canal, dataBase, codigoSTI, cotista);
			qtdExecucoes++;
		}

	}

	private void enviar(Usuario usuario, Servico servico, Canal canal,
			String dataBase, String codigoSTI, String cotista) {
		if (Servico.EXTRATO == servico) {
			enviarExtrato(usuario, canal, dataBase, codigoSTI, cotista);
		} else if (Servico.PL_COTA == servico) {
			enviarPlCota(usuario, canal, dataBase, codigoSTI);
		} else if (Servico.INFO_ANBIMA == servico) {
			enviarInfoAnbima(usuario, canal, dataBase, codigoSTI);
		} else if (Servico.POSICAO_ATIVOS == servico) {
			enviarPosicaoAtivos(usuario, codigoSTI, dataBase);
		}
	}

	private void enviarExtrato(Usuario usuario, Canal canal, String dataBase,
			String codigoSTI, String cotista) {
		if (Canal.PORTAL == canal) {
			enviarExtrato(usuario, codigoSTI, dataBase, cotista);
		} else if (Canal.ARQUIVO == canal) {
			enviarExtratoArquivo(usuario, codigoSTI, dataBase, cotista);
		}
	}

	private void enviarExtrato(Usuario usuario, String codigoSTI,
			String dataBase, String cotista) {
		try {
			TelaHome telaHome = new TelaHome(usuario);
			TelaEnvioExtrato telaEnvioExtrato = (TelaEnvioExtrato) telaHome
					.acessarSubMenu(SubMenu.ENVIA_EXTRATO);
			telaEnvioExtrato = new TelaEnvioExtrato(dataBase, codigoSTI,
					cotista);
			telaEnvioExtrato.enviar();
		} catch (Exception e) {
			Assert.fail("Erro ao enviar extrato pelo portal."+e.getMessage());
		}
	}

	private void enviarExtratoArquivo(Usuario usuario, String codigoSTI,
			String dataBase, String cotista) {
		try {
			TelaHome telaHome = new TelaHome(usuario);
			TelaEnvioExtrato telaEnvioExtrato = (TelaEnvioExtrato) telaHome
					.acessarSubMenu(SubMenu.ENVIA_EXTRATO);
			telaEnvioExtrato = new TelaEnvioExtrato();

			TelaEnvioExtratoArquivo telaEnvioExtratoArquivo = telaEnvioExtrato
					.clicarUpload(usuario, dataBase, codigoSTI, cotista);
			telaEnvioExtratoArquivo.enviar();
			telaEnvioExtratoArquivo.clicarBotaoVoltar();
		} catch (Exception e) {
			Assert.fail("Erro ao enviar extrato por arquivo."+e.getMessage());
		}
	}

	private void enviarPlCota(Usuario usuario, Canal canal, String dataBase,
			String codigoSTI) {
		if (Canal.PORTAL == canal) {
			enviarPlCotaPortal(usuario, codigoSTI, dataBase);
		} else if (Canal.ARQUIVO == canal) {
			enviarPlCotaArquivo(usuario, codigoSTI, dataBase);
		}
	}

	private void enviarPlCotaPortal(Usuario usuario, String codigoSTI,
			String dataBase) {
		try {
			TelaHome telaHome = new TelaHome(usuario);
			TelaEnvioPlCota telaEnvioPlCota = (TelaEnvioPlCota) telaHome
					.acessarSubMenu(SubMenu.ENVIA_PL_COTA);
			telaEnvioPlCota = new TelaEnvioPlCota(codigoSTI, dataBase);
			telaEnvioPlCota.enviar();
		} catch (Exception e) {
			Assert.fail("Erro ao enviar plcota pelo portal."+e.getMessage());
		}
	}

	private void enviarPlCotaArquivo(Usuario usuario, String codigoSTI,
			String dataBase) {
		enviarArquivoPlCota(usuario, codigoSTI, dataBase,
				ConstantesTestes.CAMINHO_XML_ENVIO_PL_COTA);
	}

	private void enviarInfoAnbima(Usuario usuario, Canal canal,
			String dataBase, String codigoSTI) {
		if (Canal.PORTAL == canal) {
			enviarInfoAmbina(usuario, codigoSTI, dataBase);
		} else if (Canal.ARQUIVO == canal) {
			enviarInformacaoAnbimaArquivo(usuario, codigoSTI, dataBase);
		}
	}

	private void enviarInfoAmbina(Usuario usuario, String codigoSTI,
			String dataBase) {
		try {
			TelaHome telaHome = new TelaHome(usuario);
			telaHome.acessarSubMenu(SubMenu.ENVIA_PL_COTA);
			TelaEnvioInfoAnbimaPortal telaEnvioInfoAnbimaPortal = new TelaEnvioInfoAnbimaPortal(
					codigoSTI, dataBase);
			telaEnvioInfoAnbimaPortal.enviar();
		} catch (Exception e) {
			Assert.fail("Erro ao enviar informacao anbima pelo portal.");
		}
	}

	private void enviarInformacaoAnbimaArquivo(Usuario usuario,
			String codigoSTI, String dataBase) {
		enviarArquivoPlCota(usuario, codigoSTI, dataBase,
				ConstantesTestes.CAMINHO_XML_ENVIO_INFO_ANBIMA);
	}

	private void enviarArquivoPlCota(Usuario usuario, String codigoSTI,
			String dataBase, String caminho) {
		try {
			TelaHome telaHome = new TelaHome(usuario);
			TelaEnvioPlCota telaEnvioPlCota = (TelaEnvioPlCota) telaHome
					.acessarSubMenu(SubMenu.ENVIA_PL_COTA);
			telaEnvioPlCota = new TelaEnvioPlCota(codigoSTI, dataBase);

			TelaEnvioPlCotaArquivo telaEnvioPlCotaArquivo = telaEnvioPlCota
					.clicarUploadArquivo(usuario, caminho, codigoSTI, dataBase,
							Operacao.ENVIO_INFORMES);
			telaEnvioPlCotaArquivo.enviar();
			telaEnvioPlCotaArquivo.clicarBotaoVoltar();
		} catch (Exception e) {
			Assert.fail("Erro ao enviar informacao anbima por arquivo.");
		}
	}

	private void enviarPosicaoAtivos(Usuario usuario, String codigoSTI,
			String dataBase) {
		TelaHome telaHome = new TelaHome(usuario);
		TelaEnvioPosicaoArquivo telaEnvioPosicaoArquivo = (TelaEnvioPosicaoArquivo) telaHome
				.acessarSubMenu(SubMenu.ENVIA_POSICAO);
		telaEnvioPosicaoArquivo = new TelaEnvioPosicaoArquivo(usuario,
				dataBase, codigoSTI);
		telaEnvioPosicaoArquivo.enviar();
	}

}